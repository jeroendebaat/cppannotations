#include <concepts>
#include <iostream>

class Mover
{
    public:
        Mover() = default;
        Mover(Mover const &other);
        Mover(Mover &&tmp);

        Mover &operator=(Mover &&tmp);
};

template <std::movable Type>
void fun(Type &&type)
{}

int main()
{
    fun(Mover{});
}

#include <string>
#include <iostream>

using namespace std;

template <typename Type>
concept Add =
    requires(Type lhs, Type rhs)
    {
        lhs + rhs;
    };

template <Add Type>
Type add(Type const &lhs, Type const &rhs)
{
    return lhs + rhs;
}

int main()
{
    add(1, 2);
    add("s1"s, "s2"s);

    add(cout, cerr);
}
